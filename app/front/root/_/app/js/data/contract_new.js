////////////////////////////////////////////////////////////////////////////////

$_DO.update_contract_new = async function (e) {

    let f = w2_popup_form ()

    let data = f.values ().actual ().validated ()
        
    f.lock ()

    let item = await response ({action: 'create', id: new_uuid ()}, {data})

	w2_close_popup_reload_grid ()

    console.log('created', data)
    // w2_confirm_open_tab ('Договор создан. Открыть его карточку?', '/contract/' + item.uuid)

}

////////////////////////////////////////////////////////////////////////////////

$_GET.contract_new = async function (o) {

    return $('body').data ('contracts')
    
}