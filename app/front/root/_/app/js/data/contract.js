$_DO.delete_contract = async function (e) {
    let f = w2_popup_form ()
    await response ({type: 'contracts', action: 'update', id: f.original.id}, {
        data: {is_deleted: 1}
    })

	w2_close_popup_reload_grid ()
}

$_DO.update_contract = async function (e) {

    let f = w2_popup_form ()

    let data = f.values ().actual ().validated ()
        
    f.lock ()

    let item = await response ({type: 'contracts', action: 'update', id: f.original.id}, {data})

	w2_close_popup_reload_grid ()
}
$_GET.contract = async function (o) {

    let data = await response ({type: 'contracts', id: o.id})
    
    $('body').data ('contract', data)


    return data

}