$_DRAW.contracts = async function (data) {

    $('title').text ('Договоры системы')

    $('main').w2regrid ({ 
    
        name: 'contractsGrid',             
        
        show: {
            toolbar: true,
            footer: true,
            toolbarAdd: true,
        },            

        columns: [                
            {field: 'id',   caption: 'id',    size: 100, sortable: true},
            {field: 'number',   caption: 'Номер',  size: 50,  sortable: true},
            {field: 'date_start', caption: 'Дата начала действия',   size: 50,  sortable: true},
            {field: 'date_end',    caption: 'Дата окончания действия', size: 50,  sortable: true},
        ],
                    
        url: '_back/?type=contracts',

        onAdd:      ( ) => show_block ('contract_new'),

        onDblClick: (e) => show_block (`contract`, {id: e.recid}),

    }).refresh ();
    
    $('#grid_usersGrid_search_all').focus ()

}