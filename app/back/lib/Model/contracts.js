module.exports = {

    label : 'Договоры',

    columns : {
        id: 'uuid=uuid_generate_v4()                              // Роль',           
        is_deleted: 'int=0                                     // 1, если удалён', 
        number: 'string!                                     // номер договора', 
        date_start: 'datetime!                                     // Дата начала действия', 
        date_end: 'datetime!                                     // Дата окончания действия', 
    },
    pk: 'id',


    data : [
    ],
    

}