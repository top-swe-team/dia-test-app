FROM node:10.24.1

RUN npm i -g grunt
WORKDIR /var/projects/elu_dia_w2ui_template
# RUN chmod -R +x /var/projects/elu_dia_w2ui_template/
# RUN cd back && npm i

CMD cd back && npm i && cd ../front && npm i && grunt build; grunt clean:gz; grunt
# CMD cd front && grunt build; grunt clean:gz; grunt
